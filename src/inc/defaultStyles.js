/**
 * @author Tatsumi Suzuki - 2018-08-17
 * @description Simple helper for default styles for usage in styled components.
 * @copyright http://tatsumi.eu
 */

const defaultStyles = {
	mobileBreakpoint: '767px',
	// Margins
	marginSM: '5px',
	marginDF: '15px',
	marginMD: '30px',
	marginLG: '45px',
	marginXL: '60px',
	// Font sizes
	fontSM: '14px',
	fontDF: '16px',
	fontMD: '18px',
	// Colors
	appBG: '#FAFAFA',
	appBG2: '#FFFFFF',
	appBG3: '#f2f2f2',
	appFont: '#323f45',
	appYellow: '#fbb913'
};

export default defaultStyles;
