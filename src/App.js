import React, { Component } from 'react';

import Header from "./components/Header";
import EpgContainer from "./containers/EpgContainer";

class App extends Component {

	render() {
		return (
			<div>
				<Header/>
				<div className="page-wrapper">
					<EpgContainer />
				</div>
			</div>
		);
	}
}

export default App;
