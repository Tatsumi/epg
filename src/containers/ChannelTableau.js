/**
 * @author Tatsumi Suzuki - 2018-08-17
 * @description Tableau component using Show components in a list.
 * @copyright http://tatsumi.eu
 */

import React, { Component } from 'react';

import Show from '../components/Show';

import applicationStyles from '../inc/defaultStyles';
import styled from 'styled-components';

const TableauWrapper = styled.div`
	display: flex;
	justify-content: flex-start;
`;

const ChannelTableauWrapper = styled.ul`
	margin: auto 0;
	display: inline-flex;
	padding-left: ${window.innerWidth >= 1024 ? '415px' : (window.innerWidth - (55 + 90)) / 2 + 'px'};
	flex: 1;
	background-color: ${applicationStyles.appBG3};
`;

export default class ChannelTableau extends Component {

	render() {

		const channel = this.props.children;

		return (
			<TableauWrapper ref={(node) => this.tableau = node}>
				<ChannelTableauWrapper>
					{
						Object.keys(channel.schedules).map((show) => (
							<li key={'show-' + show}>
								<Show>{ channel.schedules[show] }</Show>
							</li>
						))
					}
				</ChannelTableauWrapper>
			</TableauWrapper>
		);
	}
}
