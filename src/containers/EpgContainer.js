/**
 * @author Tatsumi Suzuki - 2018-08-17
 * @description EPG component rendering channels and shows if possible.
 * @copyright http://tatsumi.eu
 */

import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import axios from 'axios';
import moment from 'moment';

import ChannelTableau from './ChannelTableau';

import applicationStyles from '../inc/defaultStyles';
import styled from 'styled-components';

const TableauWrapper = styled.main`
	overflow-x: scroll;
	position: relative;
	padding-top: 15px;
`;

const TimeWrapper = styled.div`
	position: absolute;
	top: 0;
	left: 0;
`;

const TimeInnerWrapper = styled.ul`
	margin-left: ${window.innerWidth >= 1024 ? '415px' : (window.innerWidth - (55 + 90)) / 2 + 'px'};
	display: inline-flex;
	box-sizing: border-box;
	
	
	li {
		width: 600px;
		height: 15px;
		padding-left: 15px;
		border-right: 1px solid ${applicationStyles.appYellow};
		box-sizing: border-box;
	}
`;

const ChannelTableauWrapper = styled.div`
	margin: ${applicationStyles.marginSM} 0;
	border-radius: 3px;
`;

const ColumnWrapper = styled.div`
	display: flex;
	justify-content: space-between;
	position: relative;
	z-index: 0;
	
	aside {
		margin-top: 15px;
	}
`;

const TimeNeedle = styled.span`
	width: 2px;
	height: auto;
	position: absolute;
	top: 0;
	bottom: 0;
	left: calc(50% + 1px);
	background-color: ${applicationStyles.appYellow};
	z-index: 1337;
`;

const Button = styled.button`
	margin: ${applicationStyles.marginDF} 0;
	padding: ${applicationStyles.marginSM} ${applicationStyles.marginMD};
	border: none;
	outline: none;
	cursor: pointer;
	border-radius: 3px;
	background-color: ${applicationStyles.appYellow};
	color: white;
	z-index: 1337;
`;

const LogoContainer = styled.div`
	width: 50px;
	height: 50px;
	margin: ${applicationStyles.marginSM} 0;
	padding: ${applicationStyles.marginDF};
	background-color: ${applicationStyles.appBG2};
	box-shadow: 0 0 10px ${applicationStyles.appBG3};
	
	@media (max-width: ${applicationStyles.mobileBreakpoint}) {
		padding: ${applicationStyles.marginSM};
		width: 45px;
		height: 45px;
	}
`;

const Logo = styled.span`
	width: 100%;
	height: auto;
	padding-top: 100%;
	display: block;
	background-repeat: no-repeat;
	background-size: contain;
	background-position: center center;
`;

export default class EpgContainer extends Component {

	constructor(props) {
		super(props);

		this.state = {
			errors: false,
			channels: {}
		};

		this.scrollable = React.createRef();
	}

	/**
	 * Fetch channels and shows. Show error message if not possible to render any.
	 */
	componentDidMount() {

		axios
			.get('http://localhost:1337/epg')
			.catch(() => {

				this.setState({
					errors: true
				});

			})
			.then((response) => {

				if(typeof response !== 'undefined' &&
					response.hasOwnProperty('data') &&
					response.data.hasOwnProperty('channels')) {

					this.setState({
						errors: false,
						channels: response.data.channels
					});

					this._scroll();
				}

			});
	}

	/**
	 * Push readable hours into an array and return a timeline.
	 */
	_renderHours() {

		let hours = [];

		for(let i = 0; i < 24; i++) {

			if(i < 10) {
				hours.push('0' + i + ':00')
			} else {
				hours.push(i + ':00')
			}
		}

		return(
			<TimeInnerWrapper>
				{
					Object.keys(hours).map((hour) => (
						<li key={'hour-' + hour}>{ hours[hour] }</li>
					))
				}
			</TimeInnerWrapper>
		)
	}

	/**
	 * Scroll to this minute on render or when the Now button is pressed.
	 */
	_scroll() {
		const scrollableDomElm = ReactDOM.findDOMNode(this.scrollable);
		scrollableDomElm.scrollLeft = (600 * moment().format('HH')) + (moment().format('mm') * 10) - 4;
	}

	render() {

		return (
			<div>
				{
					this.state.errors ? (
						<h2>Could not load channels.</h2>
					) : null
				}
				<Button onClick={() => this._scroll()}>Now</Button>
				<ColumnWrapper>
					<TimeNeedle></TimeNeedle>
					<aside>
						{
							Object.keys(this.state.channels).map((channel) => (
								<LogoContainer key={'channel-' + channel}>
									<Logo style={{ backgroundImage: `url(${ this.state.channels[channel].images.logo })` }}></Logo>
								</LogoContainer>
							))
						}
					</aside>
					<TableauWrapper ref={(node) => {this.scrollable = node}}>
						<TimeWrapper>
							{
								this._renderHours()
							}
						</TimeWrapper>
							{
								Object.keys(this.state.channels).map((channel) => (
									<ChannelTableauWrapper key={'channel-' + channel}>
										<ChannelTableau>{ this.state.channels[channel] }</ChannelTableau>
									</ChannelTableauWrapper>
								))
							}

					</TableauWrapper>
				</ColumnWrapper>
			</div>
		);
	}
}
