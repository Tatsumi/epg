/**
 * @author Tatsumi Suzuki - 2018-08-17
 * @description Header component with logo.
 * @copyright http://tatsumi.eu
 */

import React, { Component } from 'react';
import logo from '../inc/logo.png';
import applicationStyles from '../inc/defaultStyles';
import styled from 'styled-components';

const HeaderWrapper = styled.header`
	padding: ${applicationStyles.marginDF} 0;
	text-align: center;
	background-color: ${applicationStyles.appBG2};
`;

const Logo = styled.img`
	width: 70px;
	
	@media (max-width: ${applicationStyles.mobileBreakpoint}) {
		width: 35px;
	}
`;

export default class Header extends Component {

	render() {
		return (
			<HeaderWrapper>
				<h1>
					<Logo src={logo} alt="Norigin Media"/>
				</h1>
			</HeaderWrapper>
		);
	}
}
