/**
 * @author Tatsumi Suzuki - 2018-08-17
 * @description Tableau show component displaying title, time and if the show is playing now or not.
 * @copyright http://tatsumi.eu
 */

import React, { Component } from 'react';

import moment from 'moment';

import applicationStyles from '../inc/defaultStyles';
import styled from 'styled-components';

const ShowWrapper = styled.div`
	height: 80px;
	padding: 0 ${applicationStyles.marginDF};
	display: flex;
	flex-direction: column;
	justify-content: center;
	border-right: 1px solid ${applicationStyles.appYellow};
	box-sizing: border-box;
	position: relative;
	z-index: 0;
	
	@media (max-width: ${applicationStyles.mobileBreakpoint}) {
		height: 55px;
	}
`;

const Title = styled.h3`
	font-size: ${applicationStyles.fontDF};
	font-weight: 600;
	display: block;
	z-index: 2;
`;

const Time = styled.p`
	margin-top: ${applicationStyles.marginSM};
	font-size: ${applicationStyles.fontSM};
	color: ${applicationStyles.appYellow};
	z-index: 2;
`;

const Duration = styled.span`
	position: absolute;
	top:0;
	bottom: 0;
	left: 0;
	background-color: rgba(251, 185, 19, 0.05);
	z-index: 1;
`;

export default class Show extends Component {

	render() {

		// Calculate show width by duration minutes * 10.
		const duration = moment.duration(moment(this.props.children.end).diff(moment(this.props.children.start)));
		const showLength = duration.asMinutes() * 10;

		return (
			<ShowWrapper style={{width: showLength}}>
				{
					// If the show is within moments HH:mm, then add overlay.
					moment(this.props.children.start).format('HH:mm') <= moment().format('HH:mm') &&
					moment(this.props.children.end).format('HH:mm') >= moment().format('HH:mm') ? (
						<Duration style={{width: showLength}}></Duration>
					) : null
				}
				<Title>{ this.props.children.title }</Title>
				<Time>
					<span>{ moment(this.props.children.start).format('HH:mm')}</span>
					-
					<span>{ moment(this.props.children.end).format('HH:mm')}</span>
				</Time>
			</ShowWrapper>
		);
	}
}
