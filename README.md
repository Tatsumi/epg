## Introduction
This is a candidate project for Norigin Media.
The project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).<br>
The task was to fetch TV channels containing shows with start and end time and to render them as an EPG tableau.<br>
My time limit for this project was 48h. 
I decided to cut out some functionality such as clickable tableau which would then open up a preview window of the particular show.<br>
I also had to exclude the date picker.

## Design
Since I like to develop and design, Iv'e made a re-design for the project.<br/>
My idea was to create a modern flat and light design using the golden color from the Norigin logo and Google font Montserrat.
The design is responsive with smaller elements for handhelled devices and larger for desktop.

## Development
This project was developed with axios to fetch content, styled components for component styling. Css-file for global styles, moment.js for easy time handling.

## candidate-tester
run
`npm install` followed by `npm start` to get localhost port 1337 going with the api. 

## epg
return to the root folder and run
`npm install` to install packades.<br/>
run `yarn start` to start the development server and you could then open `http://localhost:3000` in your browser.<br/>

